---
- name: Install dependencies
  apt:
    name:
      - build-essential
      - cmake
      - libssl-dev
      - libcurl4-openssl-dev
      - libxml2-dev
      - libxslt-dev
      - imagemagick
      - ghostscript
      - libmagickwand-dev
      - libpq-dev
      - gsfonts

- name: Template configuration files
  template:
    src: "../templates/{{service}}/{{item}}.j2"
    dest: "~{{service}}/www/config/{{item}}"
    owner: "{{service}}"
    group: "{{service}}"
    mode: "0640"
  with_items:
    - database.yml
    - diaspora.yml

- name: Patch .ruby-version with full version number instead of the first 2 digits (2.4 → 2.4.0)
  replace:
    name: "~{{service}}/www/.ruby-version"
    regexp: '^2\.4$'
    replace: "2.4.0"

- name: Setup nodejs environment
  include_role:
    name: nodejs
    tasks_from: environment-present.yml
  vars:
    pool: "{{service}}"
    options:
      create_user: False
    nodejs_manage_systemd_unit: False

- name: Allow connection to Redis
  user:
    name: "{{service}}"
    groups: redis
    append: yes

- block:

  - name: Configure bundle
    shell: ". ~/.profile && script/configure_bundler"
    args:
      chdir: "~/www"
  
  - name: Install gem dependencies
    shell: ". ~/.profile && bin/bundle install --full-index"
    args:
      chdir: "~/www"
  
  - name: Precompile assets
    shell: ". ~/.profile && RAILS_ENV=production bin/rake assets:precompile"
    args:
      chdir: "~/www"
  
  - name: Setup database schema
    shell: ". ~/.profile && RAILS_ENV=production bin/bundle exec rake db:migrate"
    args:
      chdir: "~/www"

  become_user: "{{service}}"
  when: git_checkout is changed

- name: Add systemd units
  template:
    src: "../templates/{{service}}/{{item}}.j2"
    dest: "/etc/systemd/system/{{item}}"
  with_items:
    - "{{service}}.target"
    - "{{service}}-unicorn.service"
    - "{{service}}-sidekiq.service"

- name: Enable systemd units
  systemd:
    name: "{{item}}"
    enabled: yes
    daemon_reload: yes
  with_items:
    - "{{service}}.target"
    - "{{service}}-unicorn.service"
    - "{{service}}-sidekiq.service"

- name: Start services
  service:
    name: "{{service}}.target"
    state: started

- name: Add logrotate configuration
  template:
    src: "../templates/{{service}}/logrotate"
    dest: "/etc/logrotate.d/{{service}}"
