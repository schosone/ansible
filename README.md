# Ansible

Le dépôt contient les rôles et playbooks Ansible utilisés pour configurer
entièrement l'infrastructure des Services FACiLes.

Ils peuvent être réutilisés en partant d'une installation fraiche de Debian
stretch.

Les instructions suivantes documentent la préparation de son environnement pour
exécuter les playbooks, comment sont organiser les différents rôles et
playbooks au sein du dépôt ainsi que les instructions pour rouler les tests
avec `molecule`.

## Installation de Ansible

Ansible évolue rapidement et il est recommandé d'avoir la dernière version en
date. On travaille actuellement avec la version 2.7.x. L'installation se fait
via `pip` :

```
$ pip install --user ansible
```

Il faut également installer les modules Python supplémentaires, requit pour
certains modules Ansible :

```
# apt install python-dnspython python-apt python-netaddr
```

Note : vous devrez peut-être rajouter `$HOME/.local/bin` à votre $PATH si ce
n'est pas déjà le cas. Dans votre `.bashrc`, ajoutez la ligne suivante :

```
PATH="$HOME/.local/bin:$PATH"
```

## Récupération des playbooks et rôles FACiL

```
$ git clone ssh://git@gitlab.com/facil/ansible.git
$ git clone ssh://git@gitlab.com/facil/ansible-vars.git
```

Vous devez au préalable avoir votre clé SSH publique autorisée auprès de Gitlab
pour cloner le dépôt *ansible-vars.git* qui contient les variables privées
spécifiques à l'infra. Demandez à un contributeur au projet de vous ajouter.

Les deux dépôts doivent être clonés au même niveau à cause des liens entre les
deux :

```
$ tree -L 1 ~/src/facil/
~/src/facil/
├── ansible
└── ansible-vars
```

## Organisation des dépôts

### *ansible.git*

C'est un dépôt public qui contient les playbooks et rôles génériques.
**Attention à ne pas y commiter d'informations confidentielles !**

Il est organisé de manière standard par rapport aux meilleures pratiques
Ansible, à savoir :

* `site.yml` : playbook principal, il applique les différents rôles aux groupes
  de l'inventaire ;
* `roles/` : contient les différents rôles, un par répertoire ;
* `playbooks` : contient des playbooks divers, indépendant du `site.yml` qui
  font appels à des rôles dans `roles/` ;
* `tasks/` : contient des fichiers de tâches simples, qui ne sont pas des
  rôles, inclus par les playbooks. Actuellement il contient par exemple les
  tâches pour commiter dans etckeeper (après chaque exécution d'un play) et les
  tâches spécifiques pour l'installation des différents Services FACiLes
  déployés ;
* `ansible.cfg` : fichier de configuration Ansible, automatiquement pris en
  compte quand Ansible est lancé depuis le même répertoire.

### *ansible-vars.git*

C'est un dépôt privé qui contient essentiellement l'inventaire et les variables
privées.

* `vars/` : contient des variables globales, qui n'ont pas forcement de lien
  avec un serveur ;
* `inventory/hosts` : fichier d'inventaire des machines, rangées par groupes ;
* `inventory/group_vars/` : variables Ansible qui s'appliquent aux groupes ;
* `inventory/host_vars/` : variables Ansible qui s'appliquent à un serveur en
  particulier.

## Exécution des playbooks

Les playbooks sont toujours à exécuter en se trouvant à la racine du dépôt
*ansible.git*, soit, dans l'exemple plus haut, `~/src/facil/ansible/`.

Premièrement assurez vous que vous puissiez accéder à toutes les machines
(empreinte de la clé SSH de l'hôte vérifiée, connexion par clé publique, mot de
passe sudo fonctionnel).

Si tout est ok, la commande suivante ne devrait pas renvoyer d'erreur :

```
$ ansible -m ping all
```

Exécution du playbook principal :

```
~/src/facil/ansible$ ansible-playbook -K site.yml
```

On peut rajouter l'option `-t` pour ne jouer que certains rôles ou tâches ou
l'option `-l` pour limiter l'exécution à certains hôtes seulement.

## ARA

[ARA](https://github.com/openstack/ara) est un outil permettant de récupérer
les données relatives à l'exécution des playbooks Ansible et de les présenter
de manière plus lisible et accessible via une interface web ou CLI. Son utilisation est totalement optionnel mais aide à déboguer ses playbooks.

```
$ pip install --user ara
$ source <(python -m ara.setup.env)
```

On peut ensuite exécuter les playbooks de manière ordinaire. Pour consulter les données d'exécution, lancer le serveur web puis rendez vous sur http://127.0.0.1:9191/.

```
$ ara-manage runserver
```

## Dépendances entre les rôles

![Graphique des dépendances inter-rôles](https://gitlab.com/facil/ansible/raw/master/doc/ansible-role-dependencies.png)

*Graphe généré par [`ansible-role-graph`](https://github.com/sebn/ansible-roles-graph).*

```
$ pip2 install --user ansible-roles-graph
$ ansible-roles-graph -o doc/ansible-role-dependencies.png roles/
```

## Automatisation des tests avec molecule

molecule est un outil qui permet de vérifier et tester des rôles et playbooks
Ansible.

* Voir la documentation officielle (un peu brute pour débuter cependant) :
  https://molecule.readthedocs.io/en/latest/
* Un très bon article (à peu près à jour) pour bien démarrer :
  https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule

On utilise molecule pour vérifier la conformité des fichiers YAML (yamllint
puis ansible-lint) et exécuter le rôle dans un conteneur Docker vierge (2 fois,
pour le test d'idempotence). molecule permet également de mener des tests
croisés avec différentes version de Ansible, Python et de distributions cibles,
ainsi que de faire appel à des outils de vérification tel que Goss, Inspec et
Testinfra, mais on ne les utilise pas (encore).

Il n'est pas indispensable de l'installer mais fortement recommandé pour aider
au développement. Il est également conseillé de tester ses modifications en
local avant de les pousser sur Gitlab.

### Installation

```
$ pip install --user molecule docker
```

Vous devez également avoir Docker installé sur votre système.

### Utilisation

`molecule` doit toujours être exécuter en étant dans le répertoire du rôle
(contenant le répertoire _molecule/_ :

```
$ cd roles/<rôle>/
```

* Vérifier la syntaxe avec yamllint et ansible-lint :

```
$ molecule lint
```

* Tester le rôle dans un conteneur :

```
$ molecule converge
```

molecule va partir un nouveau conteneur ou réutiliser le précédent s'il n'a pas
été supprimé. On peut se connecter au conteneur pour vérifier manuellement ce
qui a été fait :

```
$ molecule login
```

molecule exécute le playbook *molecule/default/playbook.yml* qui, par défaut,
applique le rôle en question, mais on peut le modifier pour rajouter des
dépendances ou définir des variables.

* Tester uniquement un tag (on peut passer n'importe quelle option de
  `ansible-playbooks`) :

```
$ molecule converge -- --tags foo
```

* Rouler toute la suite de tests :

```
$ molecule test
```

La sous-commande `test` va faire appel aux autres sous-commandes de molecule
pour vérifier la syntaxe du rôle, démarrer un conteneur, exécuter le rôle,
vérifier l'idempotence, vérifier la conformité du système après exécution, puis
détruire le conteneur.

## Automatisation avec Gitlab CI

Tous les rôles n'ont pas encore leurs tests molecule, mais ceux qui les ont un
job Gitlab CI pour les exécuter automatiquement.

Ils sont définit dans le fichier *.gitlab-ci.yml* à la racine du dépôt.

Pour tester le .gitlab-ci.yml en local :

```
$ gitlab-runner exec docker <nom du job>
```

(le paquet _gitlab-runner_ doit bien sûr être installé au préalable sur sa machine).

## Licence

Les rôles et playbooks Ansible de ce dépôt sont publiés sont la licence GPL v3.
