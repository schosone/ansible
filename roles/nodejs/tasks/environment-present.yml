---
- name: Create Unix account
  user:
    name: "{{options.user |default(pool)}}"
    comment: "User for {{domains |first}}"
    home: "{{options.wwwdir |default('/var/www/' ~ pool)}}"
    shell: /bin/bash
  when: options.create_user
  tags: nodejs

- name: Set umask for Unix account
  lineinfile:
    name: "~/.profile"
    regexp: "^#?umask .*$"
    line: "umask 027"
  become_user: "{{options.user |default(pool)}}"
  when: options.create_user
  tags: nodejs

- name: Create www directory with group access
  file:
    name: "{{item}}"
    state: directory
    owner: "{{options.user |default(pool)}}"
    group: "{{options.user |default(pool)}}"
    mode: "0750"
  with_items:
    - "{{options.wwwdir |default('/var/www/' ~ pool)}}"
    - "{{options.wwwdir |default('/var/www/' ~ pool ~ '/www/')}}"
  when: options.create_user
  tags: nodejs

- name: Add www-data to {{options.user |default(pool)}} group
  user:
    name: www-data
    groups: "{{options.user |default(pool)}}"
    append: yes
  when: options.create_user
  tags: nodejs

- name: Append created users to mail aliases
  lineinfile:
    name: /etc/aliases
    line: "{{options.user |default(pool)}}: root"
  notify: Renew aliases
  when: options.create_user
  tags: nodejs

- name: Clone nvm repository
  git:
    name: https://github.com/creationix/nvm.git
    dest: "~/.nvm/"
    version: "{{nodejs_nvm_version}}"
    force: yes
  become_user: "{{options.user |default(pool)}}"
  tags:
    - nodejs
    - nvm

- name: Source nvm.sh in .profile
  blockinfile:
    name: "~/.profile"
    insertafter: EOF
    block: |
      export NVM_DIR="$HOME/.nvm"
      [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
      [ -n "$BASH_VERSION" ] && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" || true  # This loads nvm bash_completion
  become_user: "{{options.user |default(pool)}}"
  tags:
    - nodejs
    - nvm

- name: Resolve node version alias
  shell: ". ~/.profile && nvm version {{nodejs_version}}"
  become_user: "{{options.user |default(pool)}}"
  register: nodejs_version_unaliased
  changed_when: False
  # The command fails the first time since no node is installed yet.
  ignore_errors: True
  check_mode: no
  tags: nodejs

- name: Install node {{nodejs_version}}
  shell: ". ~/.profile && nvm install {{nodejs_version}}"
  args:
    creates: "~/.nvm/versions/node/{{nodejs_version_unaliased.stdout_lines |last}}/bin/node"
  become_user: "{{options.user |default(pool)}}"
  tags: nodejs

- name: Check current node version
  shell: ". ~/.profile && nvm exec --silent current node --version"
  become_user: "{{options.user |default(pool)}}"
  register: nodejs_version_current
  changed_when: False
  check_mode: no
  tags: nodejs

- name: Set default node version to {{nodejs_version}}
  shell: ". ~/.profile && nvm use {{nodejs_version}}"
  become_user: "{{options.user |default(pool)}}"
  when: nodejs_version_unaliased.stdout_lines |last != nodejs_version_current.stdout_lines |last
  tags: nodejs

# The module throws "ValueError: No JSON object could be decoded". It seems to
# be a bug in the module.
#- name: Install npm additional packages
#  npm:
#    name: "{{npm_additional_packages}}"
#    path: "~/www/"
#    executable: "~/.nvm/versions/node/{{nodejs_version_unaliased.stdout_lines |last}}/bin/npm"
#    global: yes
#  become_user: "{{options.user |default(pool)}}"
#  tags: nodejs
- name: Install npm additional packages
  shell: ". ~/.profile && npm install -g {{npm_additional_packages|join(' ')}}"
  become_user: "{{options.user |default(pool)}}"
  tags: nodejs

- name: Add systemd unit
  template:
    src: unit.service
    dest: "/etc/systemd/system/{{pool}}.service"
    mode: "0644"
  notify: Restart nodejs application
  when: nodejs_manage_systemd_unit
  tags: nodejs

- name: Enable systemd unit
  systemd:
    name: "{{pool}}.service"
    enabled: yes
    daemon_reload: yes
  when: nodejs_manage_systemd_unit
  tags: nodejs
