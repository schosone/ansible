---
- name: Add OpenSSH configuration
  copy:
    src: sshd_config
    dest: /etc/ssh/
    mode: 0600
  notify: Reload OpenSSH
  tags: openssh

- name: Test if SSH ED25519 key has already been regenerated
  stat:
    path: /etc/ssh/.ansible_ed25519_key_regenerated
  register: ansible_ed25519_key_regenerated
  tags: openssh

- name: Test if SSH RSA key has already been regenerated
  stat:
    path: /etc/ssh/.ansible_rsa_key_regenerated
  register: ansible_rsa_key_regenerated
  tags: openssh

- name: Remove SSH ED25519 key if we need to regenerate it.
  file:
    path: /etc/ssh/ssh_host_ed25519_key
    state: absent
  when: not ansible_ed25519_key_regenerated.stat.exists
  tags: openssh

- name: Remove SSH RSA key if we need to regenerate it.
  file:
    path: /etc/ssh/ssh_host_rsa_key
    state: absent
  when: not ansible_rsa_key_regenerated.stat.exists
  tags: openssh

- name: Regenerate SSH ED25519 key
  command: ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
  when: not ansible_ed25519_key_regenerated.stat.exists
  notify: Reload OpenSSH
  register: keygen_ed25519
  tags: openssh

- name: Regenerate SSH RSA key
  command: ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ''
  when: not ansible_rsa_key_regenerated.stat.exists
  notify: Reload OpenSSH
  register: keygen_rsa
  tags: openssh

- name: Get SSHFP DNS record
  command: "ssh-keygen -r {{ ansible_hostname }}"
  register: sshfp
  tags: openssh

- debug: msg='NEW KEY FINGERPRINT FOR ED25519 KEY IS {{keygen_ed25519.stdout_lines[4]}}'
  when: not ansible_ed25519_key_regenerated.stat.exists
  tags: openssh

- debug: msg='NEW KEY FINGERPRINT FOR RSA KEY IS {{keygen_rsa.stdout_lines[4]}}'
  when: not ansible_rsa_key_regenerated.stat.exists
  tags: openssh

- debug: msg='SSHFP records to put in your zone file {{sshfp.stdout_lines}}'
  when: not ansible_rsa_key_regenerated.stat.exists
  tags: openssh

- name: Create a file so we hold that SSH ED25519 key has been regenerated.
  copy:
    dest: /etc/ssh/.ansible_ed25519_key_regenerated
    content: ''
  tags: openssh

- name: Create a file so we hold that SSH RSA key has been regenerated.
  copy:
    dest: /etc/ssh/.ansible_rsa_key_regenerated
    content: ''
  tags: openssh
