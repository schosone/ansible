#!/bin/sh
#
# This script copy a base container filesystem to the target LV.
# It is intented to be called from Ansible.
#

set -e

TARGET="/mnt/target/"

ctn="$1"

# Mount all volumes for $ctn, starting from root volume.
mkdir -p $TARGET
mount /dev/vg0/$ctn-root $TARGET
for vol in /dev/vg0/$ctn-*; do
    vol=$(basename $vol |sed "s/${ctn}-//")
    if [ $vol != 'root' ]; then
        mkdir -p $TARGET/$vol/
        mount /dev/vg0/$ctn-$vol $TARGET/$vol/
    fi
done

# Fetch container image from images.linuxcontainers.org and write it into
# container's volumes.
/root/lxc-download.sh --name $ctn --path /dev/null --rootfs $TARGET \
    -d debian -r stretch --arch amd64

# Prepare container to execute Ansible in it.
sed -i "s/httpredir.debian.org/$DEBIAN_MIRROR/" $TARGET/etc/apt/sources.list
echo "nameserver $NAME_SERVER" >$TARGET/etc/resolv.conf
chroot $TARGET sh -c "echo $ctn >/etc/hostname"
chroot $TARGET apt update
chroot $TARGET apt install -y --no-install-recommends python openssh-server
mkdir $TARGET/root/.ssh/
cat /root/.ssh/id_*.pub >$TARGET/root/.ssh/authorized_keys

# Allow sysadmin who executes the playbook to connect to the newly created
# container via a proxycommand.
# Not ideal in a security point of view, could be improved...
cat /home/*/.ssh/authorized_keys >>$TARGET/root/.ssh/authorized_keys

# Fix ownership in container with subuid. This should be done by lxc-download.sh script
chown -R 100000 $TARGET
chgrp -R 100000 $TARGET

# Unmount all container's volumes.
for vol in /dev/vg0/$ctn-*; do
    vol=$(basename $vol |sed "s/${ctn}-//")
    if [ $vol != "root" ]; then
        umount $TARGET/$vol/
    fi
done
umount $TARGET
rmdir $TARGET
