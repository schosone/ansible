#!/bin/sh

for client in /srv/backup/; do
    [ $(basename $client) = "incs" ] && continue

    rdiff-backup --remove-older-than 1W $client /srv/backup/incs/$(basename $client)/
done
