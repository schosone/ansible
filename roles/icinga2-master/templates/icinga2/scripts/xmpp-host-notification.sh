#!/bin/sh

echo "$NOTIFICATIONTYPE: $HOSTDISPLAYNAME is $HOSTSTATE ($HOSTOUTPUT) on $LONGDATETIME -- [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT" \
    |sendxmpp -u {{sendxmpp_jid}} -p "{{sendxmpp_password}}" -j {{sendxmpp_xmpp_host}} --tls --tls-ca-path /etc/ssl/certs/ $USERJID
