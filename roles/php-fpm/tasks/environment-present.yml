---
- name: Create Unix account
  user:
    name: "{{ options.user |default(pool) }}"
    comment: "User for {{ domains |first }}"
    home: "{{ options.wwwdir |default('/var/www/' ~ pool) }}"
    shell: /bin/bash
  when: options.create_user
  tags: php-fpm

- name: Set umask for Unix account
  lineinfile:
    name: "~/.profile"
    regexp: "^#?umask .*$"
    line: "umask 027"
  become: true
  become_user: "{{ options.user |default(pool) }}"
  when: options.create_user
  tags: php-fpm

- name: Create www Unix account
  user:
    name: "{{ options.www_user |default('www-' ~ pool) }}"
    comment: "www user for {{ domains |first }}"
    home: "{{ options.wwwdir |default('/var/www/' ~ pool ~ '/www/') }}"
    shell: /bin/false
    group: "{{ pool }}"
    create_home: false
  when: options.create_user
  tags: php-fpm

- name: Create www directory with group access
  file:
    name: "{{ item }}"
    state: directory
    owner: "{{ options.user |default(pool) }}"
    group: "{{ options.user |default(pool) }}"
    mode: "0750"
  with_items:
    - "{{ options.wwwdir |default('/var/www/' ~ pool) }}"
    - "{{ options.wwwdir |default('/var/www/' ~ pool ~ '/www/') }}"
  when: options.create_user
  tags: php-fpm

- name: Add www-data to {{ options.user |default(pool) }} group
  user:
    name: www-data
    groups: "{{ options.user |default(pool) }}"
    append: true
  when: options.create_user
  tags: php-fpm

- name: Append created users to mail aliases
  lineinfile:
    name: /etc/aliases
    line: "{{ item }}: root"
  with_items:
    - "{{ options.user |default(pool) }}"
    - "{{ options.www_user |default('www-' ~ pool) }}"
  notify: Renew aliases
  when: options.create_user
  tags: php-fpm

- name: Template the FPM pool
  template:
    src: pool.j2
    dest: "/etc/php/7.0/fpm/pool.d/{{ pool }}.conf"
    mode: "0600"
  notify: Reload php-fpm
  tags: php-fpm

- name: Add munin plugin for FPM pool
  file:
    name: "/etc/munin/plugins/php_fpm_process_{{ pool }}"
    src: /usr/local/share/munin/plugins/php_fpm_process
    state: link
  notify: Restart munin-node
  tags: php-fpm
